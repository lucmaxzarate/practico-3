package practico3;
import java.math.BigDecimal;
public class Empleado{

    private String apellido;
    private String nombre;
    private int documento;
    private String domicilio;
    private BigDecimal sueldoBasico=new BigDecimal(0.0);

    public Empleado(String apellido, String nombre, int documento, String domicilio, BigDecimal sueldoBasico) {
        this.apellido = apellido;
        this.nombre = nombre;
        this.documento = documento;
        this.domicilio = domicilio;
        this.sueldoBasico = sueldoBasico;
    }

    public String getApellido() {
        return apellido;
    }

    public String getNombre() {
        return nombre;
    }

    public int getDocumento() {
        return documento;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public BigDecimal getSueldoBasico() {
        return sueldoBasico;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setDocumento(int documento) {
        this.documento = documento;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public void setSueldoBasico(BigDecimal sueldoBasico) {
        this.sueldoBasico = sueldoBasico;
    }

}




