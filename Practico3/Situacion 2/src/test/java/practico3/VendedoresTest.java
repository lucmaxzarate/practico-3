package practico3;

import org.junit.Test;
import static org.junit.Assert.*;


public class VendedoresTest{

    @Test
    public void testSueldo() {
        System.out.println("Sueldo");
        int contracto = 6;
        Vendedor vendedores = new Vendedor("Limpiadores","Lautaro",123456,"unca",1850);
        double expResult = 1961;
        double result =  vendedores.SueldoBasico(contracto);
        assertEquals(expResult, result,0.0);
    }


    
}