package practica2;
import java.util.ArrayList;

public class Cliente{
private String ApellidoResponsable;
private Long NumeroCuentaBancaria;
private int CodigoResponsable;
private ArrayList<Mascota> mascota=new ArrayList<Mascota>();
private ArrayList<Persona> integrante=new ArrayList<Persona>(); //familiar

public Cliente(String apellidoResponsable, Long numeroCuentaBancaria, int codigoResponsable,
ArrayList<Mascota> mascota,ArrayList<Persona> integrante) {
ApellidoResponsable = apellidoResponsable;
NumeroCuentaBancaria = numeroCuentaBancaria;
CodigoResponsable = codigoResponsable;
this.mascota = mascota;
this.integrante = integrante;
}

    public String getApellidoResponsable() {
        return ApellidoResponsable;
    }

    public void setApellidoResponsable(String apellidoResponsable) {
        ApellidoResponsable = apellidoResponsable;
    }

    public Long getNumeroCuentaBancaria() {
        return NumeroCuentaBancaria;
    }

    public void setNumeroCuentaBancaria(Long numeroCuentaBancaria) {
        NumeroCuentaBancaria = numeroCuentaBancaria;
    }

    public int getCodigoResponsable() {
        return CodigoResponsable;
    }

    public void setCodigoResponsable(int codigoResponsable) {
        CodigoResponsable = codigoResponsable;
    }
    
    

   
   

    public ArrayList<Mascota> getMascota() {
        return mascota;
    }

    public void setMascota(ArrayList<Mascota> mascota) {
        this.mascota = mascota;
    }

    public ArrayList<Persona> getIntegrante() {
        return integrante;
    }

    public void setIntegrante(ArrayList<Persona> integrante) {
        this.integrante = integrante;
    }

   
}