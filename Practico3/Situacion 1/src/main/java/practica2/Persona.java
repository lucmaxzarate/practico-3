package practica2;


public class Persona{ //Familiar

private String Nombre;
private String Apellido;
private Integer Dni;
private String Domicilio;
private Integer Telefono;


    public Persona(String nombre, Integer dni) {//Constructor
        Nombre = nombre;
        Dni = dni;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public Integer getDni() {
        return Dni;
    }

    public void setDni(Integer dni) {
        Dni = dni;
    }

    public String getApellido() {
        return Apellido;
    }

    public void setApellido(String apellido) {
        Apellido = apellido;
    }

    public String getDomicilio() {
        return Domicilio;
    }

    public void setDomicilio(String domicilio) {
        Domicilio = domicilio;
    }

    public Integer getTelefono() {
        return Telefono;
    }

    public void setTelefono(Integer telefono)  {
        Telefono = telefono;
    }




}