package practica2;

public class Mascota{
private String Codigo;
private String Alias;
private String Especie;
private String Raza;
private String ColordePelo;
//private int pesopromultimasvisitas;
private int pesoactual;
private HistoriaClinica historia;
private Cliente familiaPerteneciente;

    public String getCodigo() {
        return Codigo;
    }

    public void setCodigo(String codigo) {
        Codigo = codigo;
    }

    public String getAlias() {
        return Alias;
    }

    public void setAlias(String alias) {
        Alias = alias;
    }

    public String getEspecie() {
        return Especie;
    }

    public void setEspecie(String especie) {
        Especie = especie;
    }

    public String getRaza() {
        return Raza;
    }

    public void setRaza(String raza) {
        Raza = raza;
    }

    public String getColordePelo() {
        return ColordePelo;
    }

    public void setColordePelo(String colordePelo) {
        ColordePelo = colordePelo;
    }
/*
    public int getPesopromultimasvisitas() {
        return pesopromultimasvisitas;
    }

    public void setPesopromultimasvisitas(int pesopromultimasvisitas) {
        this.pesopromultimasvisitas = pesopromultimasvisitas;
    }
*/
    public int getPesoactual() {
        return pesoactual;
    }

    public void setPesoactual(int pesoactual) {
        this.pesoactual = pesoactual;
    }

    public HistoriaClinica getHistoria() {
        return historia;
    }

    public void setHistoriaClinica(HistoriaClinica historia) {
        this.historia = historia;
    }

    public Cliente getFamiliaPerteneciente() {
        return familiaPerteneciente;
    }

    public void setFamiliaPerteneciente(Cliente cliente) {
        this.familiaPerteneciente = cliente;
    }

public Mascota(String codigo, String alias, String especie,
String raza, String colordePelo, int pesoactual) {
        Codigo = codigo;
        Alias = alias;
        Especie = especie;
        Raza = raza;
        ColordePelo = colordePelo;
       // this.pesopromultimasvisitas = pesopromultimasvisitas;
        this.pesoactual = pesoactual;
        
    }
    public void ingredarHistoriaClinica(HistoriaClinica historia){
        this.historia=historia;
    }
}


