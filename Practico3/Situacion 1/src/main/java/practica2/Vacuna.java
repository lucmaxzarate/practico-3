package practica2;

import java.time.LocalDate;

public class Vacuna {

private String enfermedad;
private LocalDate Fecha;//fecha colocacion

    public String getEnfermedad() {
        return enfermedad;
    }

    public void setEnfermedad(String enfermedad) {
        this.enfermedad = enfermedad;
    }

    public Vacuna(String enfermedad) {
        this.enfermedad = enfermedad;
    }

    public LocalDate getFecha() {
        return Fecha;
    }

    public void setFecha(LocalDate fecha) {
        Fecha = fecha;
    }
 

}