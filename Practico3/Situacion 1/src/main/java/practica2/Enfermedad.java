package practica2;
//redline
import java.time.LocalDate;
public class Enfermedad{
private String nombreEnfermedad;
private String Diagnostico;
private LocalDate Fecha; //fecha de reconocimiento



    public String getNombreEnfermedad() {
        return nombreEnfermedad;
    }

    public void setNombreEnfermedad(String enfermedad) {
        this.nombreEnfermedad = enfermedad;
    }
  
    public String getDiagnostico() {
        return Diagnostico;
    }

    public void setDiagnostico(String diagnostico) {
        Diagnostico = diagnostico;
    }

    public LocalDate getFecha() {
        return Fecha;
    }

    public void setFecha(LocalDate fecha) {
        Fecha = fecha;
    }

    public Enfermedad(String nombreEnfermedad, String diagnostico) {
        this.nombreEnfermedad = nombreEnfermedad;
        Diagnostico = diagnostico;
    
    }
  
}