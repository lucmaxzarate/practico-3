
package practica2;
import java.util.ArrayList;
import java.math.BigDecimal;
public class HistoriaClinica{
private ArrayList<BigDecimal> pesohistorico=new ArrayList<BigDecimal>();
private ArrayList<Enfermedad> enfermedad=new ArrayList<Enfermedad>();
private ArrayList<Vacuna> vacuna=new ArrayList<Vacuna>();
private Integer NumVisitas;

public HistoriaClinica(Integer numVisitas) {
    NumVisitas = numVisitas;
    
}
    public Integer getNumVisitas() {
        return NumVisitas;
    }

    public void setNumVisitas(Integer numVisitas) {
        NumVisitas = numVisitas;
    }

    public void agregarEnfermedad(Enfermedad enfermedades){
     enfermedad.add(enfermedades);
    }
    public void agregarVacunas(Vacuna vacunas){
     vacuna.add(vacunas);
    }

    public ArrayList<Enfermedad> getEnfermedad() {
        return enfermedad;
    }

    public void setEnfermedad(ArrayList<Enfermedad> enfermedad) {
        this.enfermedad = enfermedad;
    }

    public ArrayList<Vacuna> getVacuna() {
        return vacuna;
    }

    public void setVacuna(ArrayList<Vacuna> vacuna) {
        this.vacuna = vacuna;
    }

    public ArrayList<BigDecimal> getPesohistorico() {
        return pesohistorico;
    }

    public void setPesohistorico(ArrayList<BigDecimal> pesohistorico) {
        this.pesohistorico = pesohistorico;
    }
    
}